Feature: As a potential client i need to search in google to find a web site
#Agrego cambios para TP de clase 7,GIT

  @Smoke
  Scenario: Search by "crowdar"
    Given The client is in google page
    When The client searches for word crowdar
    Then The client verifies that results are shown properly

  @Smoke
  Scenario: Search by "A utomation"
    Given The client is in google page
    When The client searches for word Automation
    Then The client verifies that results are shown properly

  @Smoke
  Scenario: Search by "Docker"
    Given The client is in google page
    When The client searches for word Docker
    Then The client verifies that results are shown properly

  @Smoke
  Scenario: Search by "Lippia"
    Given The client is in google page
    When The client searches for word Lippia
    Then The client verifies that results are shown properly
